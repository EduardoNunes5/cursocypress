describe("Tickets", () => {
    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'))

    it("Fills all the text input fields", () => {
        const firstName = "Eduardo";
        const lastName = "Silva";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("eduardo@gmail.com");
        cy.get("#requests").type("Meat eater");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("selects two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("selects 'vip' ticket type", () => {
        cy.get("#vip").check();
    })

    it("selects 'social media' checkbox", () => {
        cy.get("#social-media").check();
    })

    it("selects 'friend', and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    })
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        cy.get("#email")
          .as("email")
          .type("email-gmail.com");

        cy.get("#email.invalid")
          .as("invalidEmail")
          .should("exist");

        cy.get("@email")
          .clear()
          .type("talkingabouttesting@gmail.com");
        
        cy.get("#email.invalid")
          .should("not.exist");  
    });

    it("fills and reset the form", () => {
        const firstName = "Eduardo";
        const lastName = "Silva";
        const fullName = `${firstName} ${lastName}`;
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("eduardo@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
          "contain",
          `I, ${fullName}, wish to buy 2 VIP tickets.`
        )

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");
        
        cy.get("button[type='reset']").click();

        cy.get("@submitButton")
          .should("be.disabled");
    });

    it("fills mandatory fields using support command", () => {
      const customer = {
        firstName: "João",
        lastName: "Silva",
        email: "joãosilva@example.com"
      };
      cy.fillMandatoryFields(customer);

      cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

      cy.get("#agree").uncheck();

      cy.get("@submitButton")
        .should("be.disabled");
    })
})